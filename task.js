const response = require("./hero.json");

const heroList = response.hero.map((item) => {
  return item.hero_name;
});

console.log("1. ", heroList);
console.log("2. ", heroList.sort());

const heroTank = response.hero
  .filter((item) => {
    return item.hero_role.includes("Tank");
  })
  .map((item) => {
    return `Nama hero ${item.hero_name} - Role ${item.hero_role}`;
  });

console.log("3. ", heroTank);

const heroCrowdControl = response.hero
  .filter((item) => {
    return item.hero_specially.includes("Crowd Control");
  })
  .map((item) => {
    return `Nama hero ${item.hero_name} - Role ${item.hero_specially}`;
  });

console.log("4. ", heroCrowdControl);
